// ==UserScript==
// @name         Add Bitbucket PR link to TeamCity PR builder
// @namespace    http://timothyjwrk/
// @version      0.3
// @description  Adds a link to the Pull Request in Bitbucket for anywhere there's a PR branch link
// @author       Timothy Jewell
// @match        https://tcdev.lendersoffice.com:8443/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    function addBitbucketPrLinkSibling(branchNameElement) {
        var $target = jQuery(branchNameElement);
        var prNumberMatch = /^\s*(\d+)\s*$/.exec($target.text());
        if (!prNumberMatch) {
            return;
        }

        var prNumber = prNumberMatch[1];
        var link = 'https://git.meridianlink.com/projects/LQB/repos/lendosoln/pull-requests/' + prNumber + '/';
        var $parent = $target.parent();
        if (!$parent.find('a[href="' + link + '"]').length) {
            var $anchorToAdd = jQuery('<a>').prop({ href: link, title: 'PR #' + prNumber + ' in Bitbucket', text: '#' + prNumber });
            $parent.append($anchorToAdd);
        }
    }
    jQuery(document).on('mouseover', '.branchName', function(e){ addBitbucketPrLinkSibling(e.target); });
    jQuery('.branchName').each(function(i, e) { addBitbucketPrLinkSibling(e); });
})();
