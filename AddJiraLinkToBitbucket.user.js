// ==UserScript==
// @name         Add Jira Link to Bitbucket
// @namespace    http://timothyjwrk/
// @author       Timothy Jewell
// @description  Replaces "LQB-1234" in the html with a link to the appropriate issue
// @version      1.1
// @grant        none
// @include      https://stashit.assistdirect.com/*
// ==/UserScript==

(function() {
  if (document.readyState !== 'complete') {
    document.addEventListener('readystatechange', replaceCaseNumbers);
  } else {
    replaceCaseNumbers();
  }

  function replaceCaseNumbers() {
    if (document.readyState !== 'complete') {
        return;
    }
    replaceExistingLinks();
    var tagNames = ['p', 'div', 'pre', 'li']; // IMPORTANT: this excludes 'a', so we don't double up tags
    var regex = /\b(LQB-\d+)\b/gi;
    var replacement = '<a href="https://jira.meridianlink.com/browse/$1">$1</a>';
    for (var ti = 0; ti < tagNames.length; ++ti) {
      var elements = document.getElementsByTagName(tagNames[ti]);
      for (var ei = 0; ei < elements.length; ++ei) {
        var element = elements[ei];
        if (element.innerHTML === element.textContent && regex.test(element.textContent)) {
          element.innerHTML = element.textContent.replace(regex, replacement);
        }
      }
    }
  }
  function replaceExistingLinks() {
    var badLinkRegex = /https?:\/\/.+\/plugins\/servlet\/jira-integration\/issues\/(LQB-\d+)/gi;
    var badLinkReplacement = 'https://jira.meridianlink.com/browse/$1';
    var elements = document.getElementsByTagName('a');
    for (var ei = 0; ei < elements.length; ++ei) {
      var existingLink = elements[ei];
      if (existingLink.href && badLinkRegex.test(existingLink.href)) {
        existingLink.href = existingLink.href.replace(badLinkRegex, badLinkReplacement);
        if (existingLink.classList) {
          existingLink.classList.remove('pr-title-jira-issues-trigger'); // This makes it a popup
          existingLink.classList.remove('pull-request-issues-trigger');
        }
      }
    }
  }
})();
