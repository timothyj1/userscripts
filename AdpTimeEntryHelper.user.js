// ==UserScript==
// @name         ADP Time Entry Helper
// @namespace    http://timothyjwrk/
// @version      0.4
// @description  Calculates the time elapsed from ADP to reduce manual work.
// @author       Timothy Jewell
// @match        https://workforcenow.adp.com/theme/index.html
// @require      https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js#md5=00fb18e232833d213389750973375f62,sha256=fe967293ad8e533dd8ad61e20461c1fe05d369eed31e6d3e73552c2231b528da
// @updateURL    https://git.meridianlink.com/users/timothyj/repos/userscripts/raw/AdpTimeEntryHelper.user.js
// @grant        none
// ==/UserScript==

/* global $, waitForKeyElements */
(function() {
    'use strict';

    // functional core
    function parseActions($div) {
        return $div.find('tr').map(function(i, tr) {
            var $cells = $(tr).find('td');
            return {
                text: $cells.eq(0).text(),
                timestamp: Date.parse($cells.eq(1).text())
            };
        });
    }
    function createIntervalsFromActions(data) {
        return data.toArray().reduce(function(accumulator, current) {
            var isOut = /\bout\s*$/ig.test(current.text); // we're going to assume that every entry is either in or out and ends with the appropriate text
            var latestInterval = accumulator.length ? accumulator[accumulator.length - 1] : null;
            if ((!latestInterval || (latestInterval.in && latestInterval.out)) && !isOut) {
                return accumulator.concat([{in:current.timestamp, out: null, totalMs: null}]);
            } else if (latestInterval && latestInterval.in && !latestInterval.out && isOut) {
                return accumulator.slice(0, accumulator.length - 1).concat([{in: latestInterval.in, out: current.timestamp, totalMs: current.timestamp - latestInterval.in}]);
            }
            return accumulator; // skip duplicated stops
        }, []);
    }
    function createMsFromTimespan(ts) {
        var totalHours = (ts.days * 24) + ts.hours;
        var totalMinutes = (totalHours * 60) + ts.minutes;
        var totalSeconds = (totalMinutes * 60) + ts.seconds;
        return (ts.isNegative ? -1 : 1) * (totalSeconds * 1000) + ts.ms;
    }
    function createTimespanFromMs(totalMs) {
        var isNegative = totalMs < 0;
        totalMs = isNegative ? -1 * totalMs : totalMs;
        var ms = totalMs % 1000;
        var totalSeconds = (totalMs - ms) / 1000;
        var seconds = totalSeconds % 60;
        var totalMinutes = (totalSeconds - seconds) / 60;
        var minutes = totalMinutes % 60;
        var totalHours = (totalMinutes - minutes) / 60;
        var hours = totalHours % 24;
        var days = (totalHours - hours) / 24;
        return { days, hours, minutes, seconds, ms, isNegative };
    }
    function toTimespanString(totalMs) {
        var ts = createTimespanFromMs(totalMs);
        var dayString = ts.days ? ts.days.toString() + ' day' + (ts.days != 1 ? 's' : '') + ' ' : '';
        return (ts.isNegative ? '-' : '') + dayString + ts.hours.toString().padStart(2, '0') + ':' + ts.minutes.toString().padStart(2, '0');
    }
    function parseTimespanString(value) {
        var results = /^\s*(-)?(?:(\d+)\s*days?)?\s*(\d+):(\d{1,2})(?::(\d{1,2})(?:.(\d*))?)?\s*$/.exec(value);
        if (!results || results.length != 7) {
            return null;
        }
        var isNegative = results[1] === '-';
        var days = +(results[2] || '0');
        var hours = +results[3];
        var minutes = +results[4];
        var seconds = +(results[5] || '0');
        var ms = +((results[6] || '0') + '00').slice(0, 3);
        return { days, hours, minutes, seconds, ms, isNegative };
    }
    function toTimeDisplay(date) {
        var hour = date.getHours();
        var hour12 = hour < 12 ? hour : hour - 12;
        return ('0' + (hour12 === 0 ? 12 : hour12)).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + (hour < 12? ' AM' : ' PM');
    }
    function calculateDisplayDataFromIntervals(intervals, desiredTotalTime) {
        var nowTime = Date.now();
        var isActive = false;
        var totalTime = 0;
        if (intervals.length) {
            isActive = !intervals[intervals.length - 1].out;
            totalTime = intervals.reduce(function(acc, curr) {
                return acc + (curr.totalMs || (nowTime - curr.in));
            }, 0);
        }
        var remainingTime = desiredTotalTime - totalTime;
        var endTime = nowTime + remainingTime;
        return {
            isActive,
            totalTimeString: toTimespanString(totalTime),
            endTimeString: toTimeDisplay(new Date(endTime)),
            remainingTimeString: toTimespanString(remainingTime),
            desiredTotalTimeString: toTimespanString(desiredTotalTime)
        };
    }
    function createLinkButton(text, onClick, titleText) {
        return $('<a>').prop('href', '#').text(text).attr('title', titleText || '').on('click', onClick);
    }
    function renderDisplayData($container, data, updateDesiredTotalTime) {
        var onEditDesiredTotalTime = function() {
            var output = null;
            while (output === null) {
                var raw = window.prompt('Desired total time?', data.desiredTotalTimeString);
                output = parseTimespanString(raw !== null ? raw : data.desiredTotalTimeString);
            }
            updateDesiredTotalTime(output);
        };
        $container.empty().append($('<table>').css('width', '50%').append($('<tbody>').append([
            $('<tr>').append([$('<td>').text('Status'), $('<td>').text(data.isActive ? 'Clocked in' : 'Clocked out')]),
            $('<tr>').append([$('<td>').text('Time worked'), $('<td>').text(data.totalTimeString)]),
            $('<tr>').append([$('<td>').append(createLinkButton('Work remaining', onEditDesiredTotalTime, 'edit desired total time')), $('<td>').text(data.remainingTimeString)]),
            $('<tr>').append([$('<td>').text('Clock out time'), $('<td>').text(data.endTimeString + (data.isActive ? '' : '*'))])
        ]))).append(data.isActive ? '' : $('<span>').text('* if started now, not counting breaks'));
    }
    function getLocalStorage(key) { return window.localStorage.getItem('timothyj/adp_time_entry_helper.' + key); }
    function setLocalStorage(key, value) {
        try {
            window.localStorage.setItem('timothyj/adp_time_entry_helper.' + key, value);
        } catch(e) {
        }
    }

    // imperative shell
    function action() {
        var $resultsDiv = $('<div>');
        var $activitiesDiv = $('#divActivities').after($resultsDiv);
        var desiredTotalTime = +getLocalStorage('desiredTotalTimespan') || createMsFromTimespan({days: 0, hours: 8, minutes: 0, seconds: 0, ms: 0, isNegative: false});
        var intervals;
        var updateDesiredTotalTime = function (v) { desiredTotalTime = createMsFromTimespan(v); setLocalStorage('desiredTotalTimespan', desiredTotalTime.toString()); };
        var readIntervalsFromDom = function() { intervals = createIntervalsFromActions(parseActions($activitiesDiv)); };
        var recalculateAndRender = function() { renderDisplayData($resultsDiv, calculateDisplayDataFromIntervals(intervals, desiredTotalTime), updateDesiredTotalTime); };
        readIntervalsFromDom();
        recalculateAndRender();
        window.setInterval(readIntervalsFromDom, 5000);
        window.setInterval(recalculateAndRender, 1000);
    }

    waitForJQuery(function() { waitForKeyElements('#divActivities', action); }); // waitForKeyElements will wait for multiple appearances, which is vital in a SPA
    function waitForJQuery(actionFunction) {
        function lookForJQuery() {
            if (typeof $ === 'function' && typeof $.fn === 'object' && typeof $.fn.jquery === 'string') {
                actionFunction();
                window.clearInterval(intervalToken);
            }
        }

        var intervalToken = window.setInterval(lookForJQuery, 300);
    }
})();
