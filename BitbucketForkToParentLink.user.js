// ==UserScript==
// @name         Bitbucket Fork-To-Parent Link
// @namespace    http://timothyjwrk/
// @version      0.1
// @description  Adds a link from a fork to its parent repo.
// @author       Timothy Jewell
// @match        https://git.meridianlink.com/*
// @updateURL    https://git.meridianlink.com/users/timothyj/repos/userscripts/raw/BitbucketForkToParentLink.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function createForkLink(projectName, projectLink, repoName, repoLink) {
        var span = document.createElement('span');
        span.append(document.createTextNode('(fork of '));
        span.append(createLinkElement(projectName, projectLink));
        span.append(document.createTextNode(' / '));
        span.append(createLinkElement(repoName, repoLink));
        span.append(document.createTextNode(')'));
        return span;
    }
    function createLinkElement(text, link) {
        var a = document.createElement('a');
        a.href = link;
        a.append(document.createTextNode(text));
        return a;
    }

    var site = 'https://git.meridianlink.com';
    var contentElement = document.getElementById('content');
    var repoNavElement = contentElement && contentElement.querySelector('.repository-breadcrumbs .aui-nav-selected');
    if (!repoNavElement) {
        return;
    }
    var projectKey = contentElement.dataset.projectkey;
    var repoName = contentElement.dataset.reponame;
    var req = new XMLHttpRequest();
    req.addEventListener('load', function() {
        if (req.status != 200) {
            console.log(req);
            return;
        }
        var repoInfo = JSON.parse(req.responseText);
        if (repoInfo.origin) {
            var forkedProjectInfo = repoInfo.origin.project;
            var forkedRepoInfo = repoInfo.origin;
            repoNavElement.append(document.createTextNode(' '));
            repoNavElement.append(createForkLink(forkedProjectInfo.name, forkedProjectInfo.links.self[0].href, forkedRepoInfo.name, forkedRepoInfo.links.self[0].href));
        }
    });
    req.open('GET', site + '/rest/api/1.0/projects/' + encodeURIComponent(projectKey) + '/repos/' + encodeURIComponent(repoName));
    req.send();
})();