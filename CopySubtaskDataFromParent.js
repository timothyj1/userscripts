// ==UserScript==
// @name         Copy Subtask Data from Parent
// @namespace    http://log.lendersoffice.com/tools
// @version      0.1
// @description  Copy common subtask data from the parent issue.
// @author       Michael Leinweaver
// @match        https://jira.meridianlink.com/browse/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var $ = jQuery;

    function addCreateSubtaskLinkHandler() {
        if (document.readyState === 'complete') {
            $('#create-subtask a').one('click', addCopyButton);
        }
    }

    function addCopyButton() {
        window.setInterval(function () {
            if ($('#copySubtaskDataFromParentButton').length === 0) {
                var $button = $('<button id="copySubtaskDataFromParentButton" class="aui-button" type="button" onclick="copySubtaskDataFromParent()">Copy data from parent</button>');
                $button.click(copySubtaskDataFromParent);
                $('#issuetype-single-select').parent().append('<br /><br />').append($button);
            }
        }, 1000);
    }

    function copySubtaskDataFromParent() {
        $('#summary').val($('#summary-val').text().trim());
        $('#fixVersions-textarea').val($('#fixVersions-field a').text().trim());
        $('#components-textarea').val($('#components-field a').text().trim());

        var originText = $('#customfield_10718-val').text().trim();
        var originTextToValue = {
            'None': '-1',
            'Client': '10695',
            'Internal': '10696',
            'Vendor': '10697',
            'DevInternal': '12209'
        };
        $('#customfield_10718').val(originTextToValue[originText] || '-1');

        var assignedTeamText = $('#customfield_10711-val').text().trim();
        var assignedTeamTextToValue = {
            'None': '-1',
            'Core and Integration': '10673',
            'Infrastructure': '10674',
            'LendingQB 2.0': '10675',
            'Security': '10676'
        };
        $('#customfield_10711').val(assignedTeamTextToValue[assignedTeamText] || '-1');

        var schedulingTypeText = $('#customfield_10721-val').text().trim();
        var schedulingTypeTextToValue = {
            'None': '-1',
            'Planned work': '10698',
            'Unplanned work': '10699'
        };
        $('#customfield_10721').val(schedulingTypeTextToValue[schedulingTypeText] || '-1');
    }

    if (document.readyState !== 'complete') {
        document.addEventListener('readystatechange', addCreateSubtaskLinkHandler);
    } else {
        addCreateSubtaskLinkHandler();
    }
})();