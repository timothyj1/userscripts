// ==UserScript==
// @name         Download Bitbucket Markdown HTML
// @namespace    http://timothyjwrk/
// @version      0.1
// @description  Adds a link to download HTML a Bitbucket markdown view
// @author       Timothy Jewell
// @match        https://stashit.assistdirect.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if (document.readyState !== 'complete') {
        document.addEventListener('readystatechange', startOnTimeout);
    } else {
        startOnTimeout();
    }

    function startOnTimeout() { setTimeout(setupDownloadLink, 300); }
    function setupDownloadLink() {
        var targets = document.querySelectorAll('.file-content');
        for (var i = 0; i < targets.length; ++i) {
            var markdownContainer = targets[i].querySelector('.content-view .markup-content');
            var linkContainer = targets[i].querySelector('.file-toolbar .secondary');
            if (!markdownContainer || !linkContainer) {
                continue;
            }

            var file = makeHtmlFile(getHtmlContent(markdownContainer), null)
            var link = createDownloadLink(getFileName(), file);
            linkContainer.prepend(link);
        }
    }
    // copied from https://stackoverflow.com/a/21016088
    function makeHtmlFile(text, existingFile) {
        var data = new Blob([text], {type: 'text/html'});

        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
        if (existingFile !== null) {
            window.URL.revokeObjectURL(existingFile);
        }

        var htmlFile = window.URL.createObjectURL(data);

        // returns a URL you can use as a href
        return htmlFile;
    }
    function createDownloadLink(fileName, fileUrl) {
        var link = document.createElement('a');
        link.setAttribute('download', fileName);
        link.href = fileUrl;
        link.textContent = 'Download';
        link.className = 'aui-button aui-button-link';
        return link;
    }
    function getFileName() {
        return window.location.href.replace(/^.+\//, '') + '.html';
    }
    function getHtmlContent(container) {
        return container ? '<!DOCTYPE html><html>' + container.innerHTML + '</html>' : null;
    }

})();