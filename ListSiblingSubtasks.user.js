// ==UserScript==
// @name         List Sibling Subtasks
// @namespace    https://log.lendersoffice.com
// @author       Michael Leinweaver
// @description  For a given subtask, lists all sibling subtasks.
// @version      0.5
// @grant        none
// @match        https://jira.meridianlink.com/browse/LQB-*
// @updateURL    https://git.meridianlink.com/users/timothyj/repos/userscripts/raw/ListSiblingSubtasks.user.js
// ==/UserScript==

(function() {
    function addSiblingsLink() {
        if (document.readyState !== 'complete') {
            return;
        }
        
        var $parentIssueLink = $('#parent_issue_summary');
        if ($parentIssueLink.length === 0) {
            return;
        }

        $('#lqbsiblingcases').remove();

        var template = `
<div id="lqbsiblingcases" class="module toggle-wrap">
<div id="lqbsiblingcases_heading" class="mod-header">
<a href="#" class="aui-button toggle-title" resolved=""><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"><g fill="none" fill-rule="evenodd"><path d="M3.29175 4.793c-.389.392-.389 1.027 0 1.419l2.939 2.965c.218.215.5.322.779.322s.556-.107.769-.322l2.93-2.955c.388-.392.388-1.027 0-1.419-.389-.392-1.018-.392-1.406 0l-2.298 2.317-2.307-2.327c-.194-.195-.449-.293-.703-.293-.255 0-.51.098-.703.293z" fill="#344563"></path></g></svg></a>
<h4 class="toggle-title">Sibling Subtasks</h4>
</div>

<div class="mod-content">
<div id="lqbsiblingcases-val" class="" >
<div class="wrap links-container" id="lqbsiblingcasescontainer">
<span id="subtaskloadingspan">Loading siblings...</span>
</div>
`;

        $(template).insertBefore('#descriptionmodule');

        var thisIssueKey = $('#key-val').attr('data-issue-key').trim();
        var parentIssueId = $parentIssueLink.attr('data-issue-key');

        var req = new XMLHttpRequest();
        req.overrideMimeType("application/json");
        req.open('GET', 'https://jira.meridianlink.com/rest/api/2/search?jql=' + encodeURIComponent('parent=' + parentIssueId + ' AND issuekey!=' + thisIssueKey), true);
        req.onload = function() {
            $('#subtaskloadingspan').hide();

            var jsonResponse = JSON.parse(req.responseText);
            var issues = jsonResponse.issues;

            if (issues.length === 0) {
                $('#lqbsiblingcasescontainer').append('<div>No sibling cases found.</div>');
                return;
            }

            issues.sort((a, b) => a.key.localeCompare(b.key));

            $('#lqbsiblingcasescontainer').append('<dl id="linksList" class="links-list" />');

            for (var i = 0; i < issues.length; ++i) {
                var className = issues[i].fields.status.name === "Done" ? 'resolution' : '';
                $('#linksList').append(
                    '<dd><div class="link-content"><p>' +
                    '<img width="16" height="16" style="margin-right: 5px" src="' + issues[i].fields.issuetype.iconUrl + '" />' +
                    '<a class="' + className + '" href="https://jira.meridianlink.com/browse/' + issues[i].key + '">' + issues[i].key + '</a> ' + issues[i].fields.summary +
                    '</p></div></dd>');
            }
        };
        req.send(null);
    }

    if (document.readyState !== 'complete') {
        document.addEventListener('readystatechange', addSiblingsLink);
    } else {
        addSiblingsLink();
    }
})();

