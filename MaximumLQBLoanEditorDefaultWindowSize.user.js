// ==UserScript==
// @name         Maximum LQB loan editor default window size
// @namespace    http://timothyjwrk/
// @version      0.1
// @description  Keeps the loan editor window size manageable on big monitors
// @author       Timothy Jewell
// @match        https://*.lendersoffice.com/los/mainhelper.aspx
// @match        https://*.lendingqb.com/los/mainhelper.aspx
// @match        http://*/LendersOfficeApp/los/mainhelper.aspx
// @match        https://*/LendersOfficeApp/los/mainhelper.aspx
// @updateURL    https://git.meridianlink.com/users/timothyj/repos/userscripts/raw/MaximumLQBLoanEditorDefaultWindowSize.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var maxWindowWidth = 1920;
    var maxWindowHeight = 1080;
    function fixupWindowManager(manager) {
        if (typeof manager.open !== 'function') {
            alert('unable to find open function to fixup');
        }
        manager.originalOpenWithoutModification = manager.open; // needs to be a member of manager to appease the use of `this` inside `open()`
        manager.open = function(name, url, isReplace, options) {
            if (options) {
                var optionsToUpdate = [...options.matchAll(/(width|height)=(\d+)px(?=,)?/gi)].map(function(matchData) {
                    var original = matchData[0];
                    var propertyName = matchData[1];
                    var originalValue = matchData[2];
                    var max;
                    if (/^width$/i.test(propertyName)) {
                        max = maxWindowWidth;
                    } else if (/^height$/i.test(propertyName)) {
                        max = maxWindowHeight;
                    }

                    var min = Math.min(max, +originalValue) || originalValue; // min may return NaN
                    if (min != originalValue) {
                        return [original, original.replace(originalValue, min)];
                    }
                });
                for (var update of optionsToUpdate) {
                    if (update) {
                        options = options.replace(update[0], update[1]);
                    }
                }
            }
            manager.originalOpenWithoutModification(name, url, isReplace, options);
        }
    }
    if (typeof window === 'object' && window && typeof window.gWindowManager === 'object' && window.gWindowManager)
    {
        fixupWindowManager(window.gWindowManager);
    }
})();
