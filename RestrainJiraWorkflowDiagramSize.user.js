// ==UserScript==
// @name         Restrain JIRA workflow diagram size
// @namespace    http://timothyjwrk/
// @version      0.1
// @description  JIRA diagrams blow up in Firefox, so this will keep them under control
// @author       Timothy Jewell
// @match        https://jira.meridianlink.com/*
// @updateURL    https://git.meridianlink.com/users/timothyj/repos/userscripts/raw/RestrainJiraWorkflowDiagramSize.user.js
// @grant        none
// ==/UserScript==

(function() {
    if (document.readyState !== 'complete') {
        document.addEventListener('readystatechange', stylePage);
    } else {
        stylePage();
    }

    function stylePage() {
        addGlobalStyle('#workflow-designer1 { max-height: 500px; }');
        function addGlobalStyle(css) {
            var head, style;
            head = document.getElementsByTagName('head')[0];
            if (!head) { return; }
            style = document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = css;
            head.appendChild(style);
        }
    }
})();
