UserScripts
=============

[ADP Time Entry Helper](../raw/AdpTimeEntryHelper.user.js)
-------------
Calculates the time elapsed from ADP to reduce manual work.

Author: Timothy Jewell


[Bitbucket Fork to Parent Link](../raw/BitbucketForkToParentLink.user.js)
------------
Adds a link to the bitbucket breadcrumbs for forks that links back to the parent repo.

Author: Timothy Jewell

[List Sibling Subtasks](../raw/ListSiblingSubtasks.user.js)
------------
For a given subtask, lists all sibling subtasks.

Author: Michael Leinweaver

[Maximum LQB loan editor default window size](../raw/MaximumLQBLoanEditorDefaultWindowSize.user.js)
-------------
When opening the LQB loan editor on a large monitor (e.g. 4K), overrides LQB's default behavior of opening the editor to nearly full screen.  Instead, it defines a maximum opened window size of 1920x1080.

Author: Timothy Jewell

[Restrain JIRA workflow diagram size](../raw/RestrainJiraWorkflowDiagramSize.user.js)
-------------
When opening a JIRA workflow diagram, there's a bug causing it to continue to expand infinitely in Firefox, which makes the popup difficult to close.  This will set a max size and let you close the popup without refreshing the page.

Author: Timothy Jewell
